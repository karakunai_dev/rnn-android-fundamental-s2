# RNN Android Fundamental S2
This is an Android project built with Kotlin, Fuel, and GSON. It uses Github API and currently working on implementing some additional features to fulfill Dicoding's Android Fundamental Course Final Project requirements.

# Build Status by Visual Stuido App Center
| Master Branch | Develop Branch |
|-|-|
| [![Build status](https://build.appcenter.ms/v0.1/apps/e4fcd522-ed14-4dc3-bc5d-c46c31b456af/branches/master/badge)](https://appcenter.ms) | [![Build status](https://build.appcenter.ms/v0.1/apps/e4fcd522-ed14-4dc3-bc5d-c46c31b456af/branches/develop/badge)](https://appcenter.ms) |

# License
Please have a look at `LICENSE`

# Links
- [Official Repo at Gitlab](https://gitlab.com/ronanharris09/rnn-android-fundamental-s2/)