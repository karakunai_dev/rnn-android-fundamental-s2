package rnn.practice.consumerapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import rnn.practice.consumerapp.adapters.RNNFavoritesListAdapter
import rnn.practice.consumerapp.databinding.ActivityMainBinding
import rnn.practice.consumerapp.interfaces.RNNSimpleViewModelStateInterface
import rnn.practice.consumerapp.models.RNNFavoriteUser
import rnn.practice.consumerapp.rnn_simpleviewmodel.RNNSimpleViewModel
import rnn.practice.consumerapp.rnn_simpleviewmodel.RNNSimpleViewModelState

class MainActivity : AppCompatActivity(), RNNSimpleViewModelStateInterface {

    private lateinit var viewBindingInstance: ActivityMainBinding
    private val rnnSimpleViewModelStateIntance: RNNSimpleViewModel by lazy {
        ViewModelProvider(this, SavedStateViewModelFactory(this.application, this)).get(RNNSimpleViewModel::class.java)
    }
    private var favoritesData: ArrayList<RNNFavoriteUser> = arrayListOf()
    var isReset = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        toggleActivityState(rnnSimpleViewModelStateIntance.currentState)

        viewBindingInstance.RVMainActivity.layoutManager = LinearLayoutManager(this@MainActivity)

        supportActionBar?.title = "ConsumerApp"
    }

    override fun onStart() {

        if (rnnSimpleViewModelStateIntance.hasNotDoneState.value!!) {
            triggerQueryFavorites()
        } else {
            toggleActivityState(RNNSimpleViewModelState.FINISH)
        }

        super.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        rnnSimpleViewModelStateIntance.setHasNotDoneState(true)

        when (item.itemId) {
            R.id.OMIRefresh -> {
                triggerQueryFavorites()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun toggleActivityState(currentState: RNNSimpleViewModelState) {
        super.toggleActivityState(currentState)

        lifecycleScope.launch(Dispatchers.Main) {
            when (currentState) {
                RNNSimpleViewModelState.INIT -> {
                    viewBindingInstance.apply {
                        RVMainActivity.visibility = View.GONE
                        PBMainActivity.visibility = View.GONE
                        IVStatusIconMainActivity.visibility = View.GONE
                        TVStatusString.visibility = View.GONE
                    }
                }
                RNNSimpleViewModelState.LOAD -> {
                    viewBindingInstance.apply {
                        PBMainActivity.visibility = View.VISIBLE
                        TVStatusString.apply {

                            if (isReset) {
                                text = resources.getString(R.string.TVStatusString_Load_ResetMode)
                            } else {
                                text = resources.getString(R.string.TVStatusString_Load_QueryMode)
                            }

                            visibility = View.VISIBLE
                        }
                        RVMainActivity.visibility = View.GONE
                        IVStatusIconMainActivity.visibility = View.GONE
                    }
                }
                RNNSimpleViewModelState.FINISH -> {

                    if (rnnSimpleViewModelStateIntance.hasNotDoneState.value!!) {
                        viewBindingInstance.apply {
                            PBMainActivity.visibility = View.GONE
                            TVStatusString.apply {
                                text = resources.getString(R.string.TVStatusString_Finish_Filled)
                                visibility = View.VISIBLE
                            }
                            RVMainActivity.visibility = View.GONE
                            IVStatusIconMainActivity.apply {

                                visibility = View.VISIBLE

                                Glide.with(this@MainActivity)
                                        .load(R.drawable.ic_baseline_done_outline_24)
                                        .into(this)
                            }

                            delay(600)
                        }
                    }

                    updateCurrentView()

                    viewBindingInstance.apply {
                        PBMainActivity.visibility = View.GONE

                        if (favoritesData.size != 0) {
                            RVMainActivity.visibility = View.VISIBLE
                            IVStatusIconMainActivity.visibility = View.GONE
                            TVStatusString.visibility = View.GONE
                        } else {
                            TVStatusString.apply {
                                text = resources.getString(R.string.TVStatusString_Finish_Empty)
                                visibility = View.VISIBLE
                            }
                            RVMainActivity.visibility = View.GONE
                            IVStatusIconMainActivity.apply {

                                visibility = View.VISIBLE

                                Glide.with(this@MainActivity)
                                        .load(R.drawable.ic_baseline_grass_24)
                                        .into(this)
                            }
                        }
                    }

                    rnnSimpleViewModelStateIntance.setHasNotDoneState(false)
                }
                RNNSimpleViewModelState.FAIL -> {
                    viewBindingInstance.apply {
                        PBMainActivity.visibility = View.GONE
                        TVStatusString.apply {
                            text = resources.getString(R.string.TVStatusString_Fail)
                            visibility = View.VISIBLE
                        }
                        RVMainActivity.visibility = View.GONE
                        IVStatusIconMainActivity.apply {

                            visibility = View.VISIBLE

                            Glide.with(this@MainActivity)
                                    .load(R.drawable.ic_baseline_sync_problem_24)
                                    .into(this)
                        }
                    }
                }
            }
        }
    }

    private fun updateCurrentView() {
        rnnSimpleViewModelStateIntance.apply {
            getFavorites().observe(this@MainActivity, { result ->
                viewBindingInstance.RVMainActivity.apply {
                    adapter = RNNFavoritesListAdapter(result) {
                        Snackbar.make(viewBindingInstance.root, resources.getString(R.string.ToastMainActivityItemViewClick, it.loginString), Snackbar.LENGTH_SHORT).show()
                    }
                }

                favoritesData = result
            })
        }
    }

    private fun triggerQueryFavorites() {
        lifecycleScope.launch(Dispatchers.IO) {
            rnnSimpleViewModelStateIntance.queryFavorites(contentResolver) {
                toggleActivityState(rnnSimpleViewModelStateIntance.currentState)
            }
        }
    }
}