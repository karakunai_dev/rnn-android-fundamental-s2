package rnn.practice.consumerapp.interfaces

import android.util.Log
import rnn.practice.consumerapp.rnn_simpleviewmodel.RNNSimpleViewModelState

interface RNNSimpleViewModelStateInterface {
    fun toggleActivityState(currentState: RNNSimpleViewModelState) {
        Log.d("RNNSimpleViewModel", "currentState is $currentState")
    }
}