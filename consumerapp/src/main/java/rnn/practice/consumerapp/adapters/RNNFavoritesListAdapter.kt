package rnn.practice.consumerapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import rnn.practice.consumerapp.databinding.FavoriteItemBinding
import rnn.practice.consumerapp.models.RNNFavoriteUser

class RNNFavoritesListAdapter(private val dataSource: ArrayList<RNNFavoriteUser>, private val stateListener: (RNNFavoriteUser) -> Unit) : RecyclerView.Adapter<RNNFavoritesListAdapter.RNNFavoritesListViewHolder>() {
    inner class RNNFavoritesListViewHolder(private val viewBindingInstance: FavoriteItemBinding) : RecyclerView.ViewHolder(viewBindingInstance.root) {
        fun getCurrent() = viewBindingInstance
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RNNFavoritesListViewHolder {
        return RNNFavoritesListViewHolder(FavoriteItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RNNFavoritesListViewHolder, position: Int) {
        holder.apply {
            itemView.setOnClickListener { stateListener(dataSource[position]) }

            getCurrent().apply {
                Glide.with(itemView)
                        .load(dataSource[position].avatarString)
                        .apply(RequestOptions().override(128, 128))
                        .into(IVFavoriteItem)

                TVUsernameFavoriteItem.text = ("@${dataSource[position].loginString}")
            }
        }
    }

    override fun getItemCount(): Int = dataSource.size
}