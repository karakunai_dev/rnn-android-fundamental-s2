package rnn.practice.consumerapp.rnn_simpleviewmodel

enum class RNNSimpleViewModelState {
    INIT, LOAD, FINISH, FAIL
}