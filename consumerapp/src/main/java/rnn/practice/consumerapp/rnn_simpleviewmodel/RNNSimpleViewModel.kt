package rnn.practice.consumerapp.rnn_simpleviewmodel

import android.content.ContentResolver
import android.net.Uri
import android.provider.BaseColumns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.delay
import rnn.practice.consumerapp.models.RNNFavoriteUser

class RNNSimpleViewModel(private val uiState: SavedStateHandle) : ViewModel() {

    private val currentFavorites = MutableLiveData<ArrayList<RNNFavoriteUser>>()
    var currentState = RNNSimpleViewModelState.INIT
    val hasNotDoneState = uiState.getLiveData("hasDoneState", true)

    companion object {
        private const val RNN_AFS2_AUTHORITY_STRING = "rnn.practice.dicodingandroidfundamentalsubmission2.providers.favorite"
        private const val RNN_AFS2_TABLENAME_STRING = "table_favorite"
        private const val RNN_AFS2_COLUMN_PRIMARY_STRING = BaseColumns._ID
        private const val RNN_AFS2_COLUMN_AVATAR_STRING = "avatarString"
        private const val SCHEME_MDOE = "content"
    }

    fun getFavorites() : LiveData<ArrayList<RNNFavoriteUser>> = currentFavorites

    fun setHasNotDoneState(boolean: Boolean) {
        uiState.set("hasDoneState", boolean)
    }

    suspend fun queryFavorites(contentResolver: ContentResolver, stateListener: (RNNSimpleViewModelState) -> Unit) = queryMode(contentResolver, stateListener, 0, false)

    suspend fun queryMode(contentResolver: ContentResolver, stateListener: (RNNSimpleViewModelState) -> Unit, queryType: Int, singleMode: Boolean) {

        stateListener(updateCurrentState(RNNSimpleViewModelState.LOAD))
        delay(1200)

        // 0 is for querying favorites be it one or a bulk of them, currently operation 0 is available only in bulk mode. will update later on.

        when (queryType) {
            0 -> {
                val cursor = contentResolver.query(Uri.Builder().scheme(SCHEME_MDOE).authority(RNN_AFS2_AUTHORITY_STRING).appendPath(RNN_AFS2_TABLENAME_STRING).build(), null, null, null)

                if (!singleMode) {
                    with(ArrayList<RNNFavoriteUser>()) {
                        cursor?.apply {
                            while (moveToNext()) {
                                this@with.add(
                                    RNNFavoriteUser(
                                        getString(getColumnIndexOrThrow(RNN_AFS2_COLUMN_PRIMARY_STRING)),
                                        getString(getColumnIndexOrThrow(RNN_AFS2_COLUMN_AVATAR_STRING))
                                    )
                                )
                            }
                        }

                        currentFavorites.postValue(this)
                    }
                }

                cursor?.close()
                stateListener(updateCurrentState(RNNSimpleViewModelState.FINISH))
            }
            else -> {
                stateListener(updateCurrentState(RNNSimpleViewModelState.FAIL))
                throw UnsupportedOperationException("queryMode() for $queryType is not supported")
            }
        }
    }

    private fun updateCurrentState(newState: RNNSimpleViewModelState) : RNNSimpleViewModelState {
        currentState = newState
        return currentState
    }
}