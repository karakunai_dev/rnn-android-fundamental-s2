package rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import rnn.practice.dicodingandroidfundamentalsubmission2.repository.RNNGithubUserFavoriteRepository

class RNNFavoriteViewModelFactory(private val currentRNNGithubUserFavoriteRepository: RNNGithubUserFavoriteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RNNFavorite::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RNNFavorite(currentRNNGithubUserFavoriteRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}