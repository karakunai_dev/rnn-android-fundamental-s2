package rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.repository.RNNGithubUserFavoriteRepository

class RNNFavorite(private val currentRNNGithubUserFavoriteRepository: RNNGithubUserFavoriteRepository) : ViewModel() {

    val getFavorites = currentRNNGithubUserFavoriteRepository.listFavorites
    private val currentFavorite = MutableLiveData<String>()

    fun getCurrentFavorite() : LiveData<String> = currentFavorite

    fun add(currentRNNGithubUserFavorite: RNNGithubUserFavorite) = viewModelScope.launch {
        currentRNNGithubUserFavoriteRepository.add(currentRNNGithubUserFavorite)
    }

    fun find(currentLoginString: String) = viewModelScope.launch {
        Log.d("RNNDB", "BEFORE ${currentFavorite.value}")
        currentFavorite.postValue(currentRNNGithubUserFavoriteRepository.find(currentLoginString)?.loginString)
        Log.d("RNNDB", "AFTER ${currentFavorite.value}")
    }

    fun delete(currentRNNGithubUserFavorite: RNNGithubUserFavorite) = viewModelScope.launch {
        currentRNNGithubUserFavoriteRepository.delete(currentRNNGithubUserFavorite)
    }

    fun reset() = viewModelScope.launch {
        currentRNNGithubUserFavoriteRepository.reset()
    }
}