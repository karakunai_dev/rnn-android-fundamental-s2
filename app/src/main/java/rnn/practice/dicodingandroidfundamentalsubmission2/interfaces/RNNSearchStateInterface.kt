package rnn.practice.dicodingandroidfundamentalsubmission2.interfaces

import android.util.Log
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search.RNNSearchState

interface RNNSearchStateInterface {

    fun toggleActivityState(currentState: RNNSearchState) {
        Log.d("RNNSearch", "currentState is $currentState")
    }
}