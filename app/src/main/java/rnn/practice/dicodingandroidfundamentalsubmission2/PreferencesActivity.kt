package rnn.practice.dicodingandroidfundamentalsubmission2

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.ActivityPreferencesBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.fragments.RNNSimpleTimePicker
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNUserPreferences
import rnn.practice.dicodingandroidfundamentalsubmission2.preferences.RNNSharedUserPreferences
import rnn.practice.dicodingandroidfundamentalsubmission2.receivers.RNNAlarmReceiver
import java.text.SimpleDateFormat
import java.util.*

class PreferencesActivity : AppCompatActivity(), RNNSimpleTimePicker.DialogTimeEventListener {

    private lateinit var viewBindingInstance: ActivityPreferencesBinding

    companion object {
        private const val RNN_USER_PREFERENCES_SELECTED_CUSTOM_TIME = "rnn_user_preferences_selected_custom_time"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityPreferencesBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        supportActionBar?.title = resources.getString(R.string.ABPreferencesActivity)

        with(RNNSharedUserPreferences(this).returnPrefs()) {
            viewBindingInstance.apply {
                SMActiveState.isChecked = this@with.isActive
                SMCustomState.isChecked = this@with.isCustomTime
                TVCurrentTimeReminderString.text = this@with.currentReminderTime
                SMActiveState.setOnClickListener {
                    toggleAlarm(SMActiveState.isChecked, TVCurrentTimeReminderString.text.toString())
                }
                SMCustomState.setOnClickListener {
                    if (SMCustomState.isChecked) {
                        RNNSimpleTimePicker().show(supportFragmentManager, RNN_USER_PREFERENCES_SELECTED_CUSTOM_TIME)

                        SMActiveState.isChecked = false
                        toggleAlarm(SMActiveState.isChecked, TVCurrentTimeReminderString.text.toString())

                        Snackbar.make(viewBindingInstance.root, resources.getText(R.string.SnackbarReminderChangedPreferencesActivity), Snackbar.LENGTH_LONG).show()
                    }
                }
                IBLaunchBatterySettings.setOnClickListener {
                    if (!(getSystemService(Context.POWER_SERVICE) as PowerManager).isIgnoringBatteryOptimizations(packageName)) {
                        startActivity(Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:${packageName}")))
                    } else {
                        Snackbar.make(root, resources.getText(R.string.SnackbarAppBatteryOptimizationWhitelistedPreferencesActivity), Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        saveCurrentPreferences()
        onBackPressed()
        return true
    }

    override fun onTimeSet(tag: String?, hourOfDay: Int, minute: Int) {
        viewBindingInstance.TVCurrentTimeReminderString.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(
            Calendar.getInstance().let {
                it.set(Calendar.HOUR_OF_DAY, hourOfDay)
                it.set(Calendar.MINUTE, minute)
                it.time
            }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.preferences_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.OMISavePreferences -> {
                saveCurrentPreferences()
                Snackbar.make(viewBindingInstance.root, resources.getText(R.string.SnackbarSharedPreferencesSaved), Snackbar.LENGTH_SHORT).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun toggleAlarm(boolean: Boolean, string: String) {
        if (boolean) {
            val currentReminderTime = string.split(":").toTypedArray()

            (getSystemService(Context.ALARM_SERVICE) as AlarmManager).setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                Calendar.getInstance().apply {
                    set(Calendar.HOUR_OF_DAY, currentReminderTime[0].toInt())
                    set(Calendar.MINUTE, currentReminderTime[1].toInt())
                    set(Calendar.SECOND, 0)
                }.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                PendingIntent.getBroadcast(
                    applicationContext,
                    0,
                    Intent(applicationContext, RNNAlarmReceiver::class.java).apply {
                        putExtra("RNN_ALARM_TITLE", resources.getString(R.string.AppReminderTitle))
                        putExtra("RNN_ALARM_MESSAGE", resources.getString(R.string.AppReminderMessage))
                        putExtra("setHour", currentReminderTime[0].toInt())
                        putExtra("setMinute", currentReminderTime[1].toInt())
                    },
                    PendingIntent.FLAG_CANCEL_CURRENT
                )
            )

            Snackbar.make(viewBindingInstance.root, resources.getText(R.string.SnackbarReminderActivePreferencesActivity), Snackbar.LENGTH_SHORT).show()
        } else {
            (getSystemService(Context.ALARM_SERVICE) as AlarmManager).cancel(
                PendingIntent.getBroadcast(
                    applicationContext,
                    0,
                    Intent(applicationContext, RNNAlarmReceiver::class.java),
                    0
                )
            )

            Snackbar.make(viewBindingInstance.root, resources.getText(R.string.SnackbarReminderInactivePreferencesActivity), Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun saveCurrentPreferences() {
        RNNSharedUserPreferences(this).updatePrefs(RNNUserPreferences(viewBindingInstance.SMActiveState.isChecked, viewBindingInstance.SMCustomState.isChecked, viewBindingInstance.TVCurrentTimeReminderString.text.toString()))
    }
}