package rnn.practice.dicodingandroidfundamentalsubmission2

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import rnn.practice.dicodingandroidfundamentalsubmission2.adapters.RNNGithubUsersListAdapter
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.ActivityMainBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.interfaces.RNNSearchStateInterface
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserSearch
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search.RNNSearch
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search.RNNSearchState
import java.util.*

class MainActivity : AppCompatActivity(), RNNSearchStateInterface {

    private lateinit var viewBindingInstance: ActivityMainBinding
    private lateinit var currentRNNGithubUserSearch: RNNGithubUserSearch
    private val rnnSearchInstance: RNNSearch by lazy {
        ViewModelProvider(this, SavedStateViewModelFactory(this.application, this)).get(RNNSearch::class.java)
    }
    private var dataSource: ArrayList<RNNGithubUserMain> = arrayListOf()
    private var hasNotDoneOnce = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        AppCenter.start(application, "583bea43-0f06-40cd-ba0c-fee712be6a4c", Analytics::class.java, Crashes::class.java)

        viewBindingInstance.RVMainActivity.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        supportActionBar?.title = resources.getString(R.string.ABMainActivity)

        toggleActivityState(rnnSearchInstance.currentState)

//        rnnSearchInstance.setCurrentContext(applicationContext)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        
        (menu as MenuBuilder).setOptionalIconsVisible(true)

        val searchManagerInstance = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchViewInstance = menu?.findItem(R.id.OMISearch)?.actionView as SearchView

        searchViewInstance.apply {

            setSearchableInfo(searchManagerInstance.getSearchableInfo(componentName))
            queryHint = resources.getString(R.string.OMISearchHint)
            maxWidth = Int.MAX_VALUE

            // I actually want to do queryUsers() on onQueryTextChange() instead but then I worried that
            // it would dry up my rate limit pretty quickly. Theoritically I can implement a timer
            // and count to n milliseconds before calling queryUsers() right when the user stops typing
            //  but then the timer has to be cancellable if the user decides to continue typing
            // and I haven't found out how should I implement that. It will surely be more neat to be able to
            // query with and without clicking enter to submit the query

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    lifecycleScope.launch(Dispatchers.IO) {
                        query?.let { rnnSearchInstance.queryUsers(it) {
                            toggleActivityState(rnnSearchInstance.currentState)
                        }}
                    }

                    closeSoftKeyboard()

                    return true
                }

                // Future Homework
                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })

            return true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.OMIAboutApp -> {
                startActivity(
                        Intent(this@MainActivity, AboutAppActivity::class.java)
                )
            }
            R.id.OMIFavorite -> {
                startActivity(
                        Intent(this@MainActivity, FavoritesActivity::class.java)
                )
            }
            R.id.OMIPreferences -> {
                startActivity(
                        Intent(this@MainActivity, PreferencesActivity::class.java)
                )
            }
        }

        return true
    }

    override fun onResume() {
        super.onResume()

        viewBindingInstance.apply {
            if (rnnSearchInstance.liveMainActivityComplete.value == true) {
                TVStatusString.visibility = View.VISIBLE
                IVStatusIconMainActivity.visibility = View.VISIBLE
            } else {
                RVMainActivity.visibility = View.VISIBLE
                TVStatusString.visibility = View.GONE
                IVStatusIconMainActivity.visibility = View.GONE
                PBMainActivity.visibility = View.GONE
            }
        }
    }

    // Do something based on the current RNNSearchState state on RNNSearch.queryUser() / .queryUsers()
    // on the main thread, mostly view related.

    override fun toggleActivityState(currentState: RNNSearchState) {
        super.toggleActivityState(currentState)

        lifecycleScope.launch(Dispatchers.Main) {
            when (currentState) {
                RNNSearchState.INIT -> {
                    viewBindingInstance.apply {
                        RVMainActivity.visibility = View.GONE
                        TVStatusString.apply {
                            text = resources.getString(R.string.TVStatusString_INIT)
                            visibility = View.VISIBLE
                        }
                        IVStatusIconMainActivity.apply{
                            visibility = View.VISIBLE
                            Glide.with(this@MainActivity)
                                    .load(R.drawable.ic_baseline_grass_24)
                                    .into(this)
                        }
                        PBMainActivity.visibility = View.GONE
                    }
                }
                RNNSearchState.LOAD -> {
                    viewBindingInstance.apply {
                        RVMainActivity.visibility = View.GONE
                        TVStatusString.apply{
                            text = resources.getString(R.string.TVStatusString_LOAD)
                            visibility = View.VISIBLE
                        }
                        IVStatusIconMainActivity.visibility = View.GONE
                        PBMainActivity.visibility = View.VISIBLE
                    }
                }
                RNNSearchState.FINISH -> {
                    updateCurrentView()

                    if (rnnSearchInstance.liveMainActivityComplete.value == true) {
                        viewBindingInstance.apply {
                            RVMainActivity.visibility = View.GONE
                            TVStatusString.apply {
                                text = resources.getString(R.string.TVStatusString_FINISH)
                                visibility = View.VISIBLE
                            }
                            IVStatusIconMainActivity.apply{
                                visibility = View.VISIBLE
                                Glide.with(this@MainActivity)
                                        .load(R.drawable.ic_baseline_done_outline_24)
                                        .into(this)
                            }
                            PBMainActivity.visibility = View.GONE
                        }

                        delay(300)
                    }

                    if (currentRNNGithubUserSearch.resultCount == 0 && dataSource.size == 0) {
                        viewBindingInstance.apply {
                            RVMainActivity.visibility = View.GONE
                            TVStatusString.apply {
                                text = resources.getString(R.string.TVStatusString_FINISH_EMPTY)
                                visibility = View.VISIBLE
                            }
                            IVStatusIconMainActivity.apply{
                                visibility = View.VISIBLE
                                Glide.with(this@MainActivity)
                                        .load(R.drawable.ic_baseline_search_off_24)
                                        .into(this)
                            }
                            PBMainActivity.visibility = View.GONE
                        }

                        Toast.makeText(this@MainActivity, resources.getString(R.string.ToastStatusString_FINISH_EMPTY), Toast.LENGTH_LONG).show()
                    } else {
                        viewBindingInstance.apply {
                            RVMainActivity.visibility = View.VISIBLE
                            TVStatusString.visibility = View.GONE
                            IVStatusIconMainActivity.visibility = View.GONE
                            PBMainActivity.visibility = View.GONE
                        }

                        if (rnnSearchInstance.liveMainActivityComplete.value == true) {
                            Toast.makeText(this@MainActivity, resources.getString(R.string.ToastStatusString_FINISH_FILLED, resources.getQuantityString(R.plurals.ResultPlurals, currentRNNGithubUserSearch.resultCount, currentRNNGithubUserSearch.resultCount), resources.getQuantityString(R.plurals.UserPlurals, dataSource.size, dataSource.size)), Toast.LENGTH_LONG).show()
                            rnnSearchInstance.setLiveMainActivityComplete(false)
                        }
                    }

                    hasNotDoneOnce = false
                }
                RNNSearchState.FAIL -> {

                    if (hasNotDoneOnce) {
                        viewBindingInstance.apply {
                            RVMainActivity.visibility = View.GONE
                            TVStatusString.apply {
                                text = resources.getString(R.string.TVStatusString_FAIL)
                                visibility = View.VISIBLE
                            }
                            IVStatusIconMainActivity.apply{
                                visibility = View.VISIBLE
                                Glide.with(this@MainActivity)
                                        .load(R.drawable.ic_baseline_sync_problem_24)
                                        .into(this)
                            }
                            PBMainActivity.visibility = View.GONE
                        }
                    }

                    if (rnnSearchInstance.liveMainActivityComplete.value == true) {
                        Toast.makeText(this@MainActivity, resources.getString(R.string.ToastStatusString_FAIL), Toast.LENGTH_LONG).show()
                        rnnSearchInstance.setLiveMainActivityComplete(false)
                    }
                }
            }
        }
    }

    // Invoke adapter to update per call with newer data
    private fun updateCurrentView() {
        viewBindingInstance.RVMainActivity.apply {

            rnnSearchInstance.getUsers().observe(this@MainActivity, {
                if (it != null) {
                    currentRNNGithubUserSearch = it
                    dataSource = it.resultUsers
                    adapter = RNNGithubUsersListAdapter(it.resultUsers) {

                        closeSoftKeyboard()

                        startActivity(
                                Intent(this@MainActivity, DetailsActivity::class.java)
                                        .putExtra(DetailsActivity.RNN_CURRENT_GITHUB_USER_MAIN, it)
                        )
                    }
                }
            })
        }
    }

    // Not necessary but it's gotta be too repetitive to call the same thing just in case
    private fun closeSoftKeyboard() {
        (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this@MainActivity.currentFocus?.windowToken, 0)
    }
}