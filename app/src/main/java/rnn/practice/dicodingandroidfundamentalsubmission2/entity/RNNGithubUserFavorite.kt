package rnn.practice.dicodingandroidfundamentalsubmission2.entity

import android.provider.BaseColumns
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = RNNGithubUserFavorite.tableName)
data class RNNGithubUserFavorite(
    @PrimaryKey
    @ColumnInfo(name = primaryColumn, index = true)
    var loginString: String,
    @ColumnInfo(name = "avatarString")
    var avatarString: String
) {
    companion object {
        const val tableName = "table_favorite"
        const val primaryColumn = BaseColumns._ID
    }
}
