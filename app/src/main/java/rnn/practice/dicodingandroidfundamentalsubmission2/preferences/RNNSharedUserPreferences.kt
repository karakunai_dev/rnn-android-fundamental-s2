package rnn.practice.dicodingandroidfundamentalsubmission2.preferences

import android.content.Context
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNUserPreferences

internal class RNNSharedUserPreferences(context: Context) {

    private val sharedPreferencesInstance = context.getSharedPreferences(RNN_USER_PREFERENCES_OBJECT, Context.MODE_PRIVATE)

    companion object {
        private const val RNN_USER_PREFERENCES_OBJECT = "rnn_user_preference_object"
        private const val RNN_USER_IS_ACTIVE = "rnn_user_is_active"
        private const val RNN_USER_IS_CUSTOM = "rnn_user_is_custom"
        private const val RNN_USER_CURRENT_TIME = "rnn_user_current_time"
    }

    fun updatePrefs(currentRNNUserPreferences: RNNUserPreferences) {
        sharedPreferencesInstance.edit()
            .putBoolean(RNN_USER_IS_ACTIVE, currentRNNUserPreferences.isActive)
            .putBoolean(RNN_USER_IS_CUSTOM, currentRNNUserPreferences.isCustomTime)
            .putString(RNN_USER_CURRENT_TIME, currentRNNUserPreferences.currentReminderTime)
            .apply()
    }

    fun returnPrefs() : RNNUserPreferences {
        return RNNUserPreferences().apply {
            with(sharedPreferencesInstance) {
                isActive = this.getBoolean(RNN_USER_IS_ACTIVE, false)
                isCustomTime = this.getBoolean(RNN_USER_IS_CUSTOM, false)
                currentReminderTime = this.getString(RNN_USER_CURRENT_TIME, "09:00")
            }
        }
    }
}