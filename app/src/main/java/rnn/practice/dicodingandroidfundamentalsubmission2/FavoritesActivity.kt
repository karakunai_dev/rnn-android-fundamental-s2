package rnn.practice.dicodingandroidfundamentalsubmission2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import rnn.practice.dicodingandroidfundamentalsubmission2.adapters.RNNGithubUsersListAdapter
import rnn.practice.dicodingandroidfundamentalsubmission2.application.RNNFavoriteApplication
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.ActivityFavoritesBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite.RNNFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite.RNNFavoriteViewModelFactory

class FavoritesActivity : AppCompatActivity() {

    private lateinit var viewBindingInstance: ActivityFavoritesBinding
    private val rnnFavoriteInstance: RNNFavorite by viewModels {
        RNNFavoriteViewModelFactory((application as RNNFavoriteApplication).repositoryInstance)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityFavoritesBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        supportActionBar?.title = resources.getString(R.string.ABFavoritesActivity)
        updateCurrentView()
    }

    override fun onResume() {
        updateCurrentView()
        super.onResume()
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favories_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.OMIResetDB -> {
                rnnFavoriteInstance.reset()
                updateCurrentView()
                Snackbar.make(viewBindingInstance.root, resources.getString(R.string.SnackbarResetFinishFavoritesActivity), Snackbar.LENGTH_LONG).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun updateCurrentView() {
        rnnFavoriteInstance.getFavorites.observe(this@FavoritesActivity, {
            with(ArrayList<RNNGithubUserMain>()) {
                if (it.isNotEmpty()) {

                    it.forEach { item ->
                        this.add(RNNGithubUserMain(loginString = item.loginString, avatarString = item.avatarString))
                    }

                    viewBindingInstance.apply {
                        IVStatusIconFavoritesActivity.visibility = View.GONE
                        TVStatusStringFavoritesActivity.visibility = View.GONE
                        RVFavoritesActivity.visibility = View.VISIBLE
                        RVFavoritesActivity.layoutManager = LinearLayoutManager(this@FavoritesActivity)
                        RVFavoritesActivity.adapter = RNNGithubUsersListAdapter(this@with) {
                            startActivity(
                                    Intent(this@FavoritesActivity, DetailsActivity::class.java)
                                            .putExtra(DetailsActivity.RNN_CURRENT_GITHUB_USER_MAIN, it)
                            )
                        }
                    }
                } else {
                    viewBindingInstance.apply {
                        RVFavoritesActivity.visibility = View.GONE
                        IVStatusIconFavoritesActivity.visibility = View.VISIBLE
                        TVStatusStringFavoritesActivity.visibility = View.VISIBLE
                    }
                }
            }
        })
    }
}