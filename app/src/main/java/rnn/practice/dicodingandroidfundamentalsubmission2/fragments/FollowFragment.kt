package rnn.practice.dicodingandroidfundamentalsubmission2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import rnn.practice.dicodingandroidfundamentalsubmission2.DetailsActivity
import rnn.practice.dicodingandroidfundamentalsubmission2.R
import rnn.practice.dicodingandroidfundamentalsubmission2.adapters.RNNGithubUsersListAdapter
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.FragmentFollowBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain

class FollowFragment : Fragment() {

    private var _viewBindingInstance: FragmentFollowBinding? = null
    private val viewBindingInstance get() = _viewBindingInstance as FragmentFollowBinding
    private var currentUserFollow: ArrayList<RNNGithubUserMain> = arrayListOf()
    private var currentFollowType = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _viewBindingInstance = FragmentFollowBinding.inflate(inflater, container, false)
        return viewBindingInstance.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currentUserFollow = arguments?.getParcelableArrayList<RNNGithubUserMain>("currentUserDataFollow") as ArrayList<RNNGithubUserMain>
        currentFollowType = arguments?.getInt("currentFollowDataMode") as Int

        viewBindingInstance.apply {
            if (currentUserFollow.size == 0) {

                root.minHeight = 1024

                RVFollowerFragment.visibility = View.GONE

                IVStatusIconFollowFragment.apply {
                    Glide.with(view)
                            .load(R.drawable.ic_baseline_grass_24)
                            .into(this)
                    visibility = View.VISIBLE
                }

                TVStatusStringFollowFragment.apply {
                    when (currentFollowType) {
                        0 -> {
                            text = resources.getString(R.string.NoFollowerString)
                        }
                        1 -> {
                            text = resources.getString(R.string.NoFollowingString)
                        }
                    }
                    visibility = View.VISIBLE
                }

            } else {
                RVFollowerFragment.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = RNNGithubUsersListAdapter(currentUserFollow) {
                        startActivity(
                                Intent(context, DetailsActivity::class.java)
                                        .putExtra(DetailsActivity.RNN_CURRENT_GITHUB_USER_MAIN, it)
                        )
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _viewBindingInstance = null
    }
}