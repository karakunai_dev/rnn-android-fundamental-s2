package rnn.practice.dicodingandroidfundamentalsubmission2.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RNNGithubUserMain(
    @SerializedName("login")
    var loginString: String? = "loginString",
    @SerializedName("id")
    var idString: String? = "idString",
    @SerializedName("avatar_url")
    var avatarString: String? = "avatarString"
) : Parcelable
