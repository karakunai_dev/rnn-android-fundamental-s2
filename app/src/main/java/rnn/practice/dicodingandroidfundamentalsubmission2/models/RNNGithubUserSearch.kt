package rnn.practice.dicodingandroidfundamentalsubmission2.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RNNGithubUserSearch(
        @SerializedName("total_count")
        var resultCount: Int = 0,
        @SerializedName("items")
        var resultUsers: ArrayList<RNNGithubUserMain>
) : Parcelable
