package rnn.practice.dicodingandroidfundamentalsubmission2.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RNNGithubUserDetails(
    @SerializedName("login")
    var loginString: String? = "loginString",
    @SerializedName("id")
    var idString: String? = "idString",
    @SerializedName("avatar_url")
    var avatarString: String? = "avatarString",
    @SerializedName("name")
    var nameString: String? = "nameString",
    @SerializedName("company")
    var companyString: String? = "companyString",
    @SerializedName("blog")
    var blogString: String? = "blogString",
    @SerializedName("location")
    var locationString: String? = "locationString",
    @SerializedName("hireable")
    var hireableStatus: Boolean = false,
    @SerializedName("bio")
    var bioString: String? = "bioString",
    @SerializedName("public_repos")
    var publicRepoCount: Int = 0,
    @SerializedName("public_gists")
    var publicGistsCount: Int = 0,
    @SerializedName("followers")
    var followersCount: Int = 0,
    @SerializedName("following")
    var followingCount: Int = 0,
    @SerializedName("created_at")
    var createdDateString: String = "2000-02-20T20:20:20Z"
) : Parcelable
