package rnn.practice.dicodingandroidfundamentalsubmission2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.interfaces.RNNGithubUserFavoriteDAO

@Database(entities = [RNNGithubUserFavorite::class], version = 1, exportSchema = false)
abstract class RNNGithubUserFavoriteRoomDatabase : RoomDatabase() {
    abstract fun currentRNNGithubUserFavoriteDAO() : RNNGithubUserFavoriteDAO

    companion object {

        const val DB_NAME = "rnn_db_favorites"

        @Volatile
        private var CURRENT_INSTANCE: RNNGithubUserFavoriteRoomDatabase? = null

        fun checkDatabase(context: Context) : RNNGithubUserFavoriteRoomDatabase {
            return CURRENT_INSTANCE ?: synchronized(this) {
                val newInstance = Room.databaseBuilder(
                    context.applicationContext,
                    RNNGithubUserFavoriteRoomDatabase::class.java,
                    DB_NAME
                ).build()
                CURRENT_INSTANCE = newInstance
                newInstance
            }
        }
    }
}