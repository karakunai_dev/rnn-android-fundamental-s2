package rnn.practice.dicodingandroidfundamentalsubmission2

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import rnn.practice.dicodingandroidfundamentalsubmission2.adapters.DetailsActivitySectionsPagerAdapter
import rnn.practice.dicodingandroidfundamentalsubmission2.application.RNNFavoriteApplication
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.ActivityDetailsBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.interfaces.RNNSearchStateInterface
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserDetails
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite.RNNFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_favorite.RNNFavoriteViewModelFactory
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search.RNNSearch
import rnn.practice.dicodingandroidfundamentalsubmission2.rnn_search.RNNSearchState

class DetailsActivity : AppCompatActivity(), RNNSearchStateInterface {

    private var currentRNNGithubUserDetails: RNNGithubUserDetails = RNNGithubUserDetails()
    private var currentRNNGithubUserMain: RNNGithubUserMain = RNNGithubUserMain()
    private var currentRNNGithubUserFollower: ArrayList<RNNGithubUserMain> = arrayListOf()
    private var currentRNNGithubUserFollowing: ArrayList<RNNGithubUserMain> = arrayListOf()
    private val rnnSearchInstance: RNNSearch by lazy {
        ViewModelProvider(this, SavedStateViewModelFactory(this.application, this)).get(RNNSearch::class.java)
    }
    private val rnnFavoriteInstance: RNNFavorite by viewModels {
        RNNFavoriteViewModelFactory((application as RNNFavoriteApplication).repositoryInstance)
    }
    private var contentLoadPhase: Int = 0
    private var normalCreate: Boolean = true

    companion object {
        @StringRes
        private val RNN_DETAILS_ACTIVITY_TAB_TITLES = intArrayOf(
                R.string.FollowerString,
                R.string.FollowingString,
        )
        const val RNN_CURRENT_GITHUB_USER_MAIN = "rnn_current_github_user_main"
    }

    private lateinit var viewBindingInstance: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        currentRNNGithubUserMain = intent.getParcelableExtra<RNNGithubUserMain>(RNN_CURRENT_GITHUB_USER_MAIN) as RNNGithubUserMain

        supportActionBar?.apply{
            elevation = 0f
            title = resources.getString(R.string.ABDetailsActivity, currentRNNGithubUserMain.loginString)
        }
    }

    override fun onStart() {

        if (rnnSearchInstance.liveDetailsActivityCreate.value == true) {

            rnnSearchInstance.setLiveDetailsActivityCreate(false)

            toggleActivityState(rnnSearchInstance.currentState)

            lifecycleScope.launch(Dispatchers.IO) {
                rnnSearchInstance.queryUser(currentRNNGithubUserMain.loginString.toString()) {toggleActivityState(rnnSearchInstance.currentState)}
                rnnSearchInstance.queryFollowers(currentRNNGithubUserMain.loginString.toString()) {toggleActivityState(rnnSearchInstance.currentState)}
                rnnSearchInstance.queryFollowing(currentRNNGithubUserMain.loginString.toString()) {toggleActivityState(rnnSearchInstance.currentState)}
            }
        } else {
            contentLoadPhase = 3
            normalCreate = false
            toggleActivityState(RNNSearchState.FINISH)
        }

        super.onStart()
    }

    override fun toggleActivityState(currentState: RNNSearchState) {
        super.toggleActivityState(currentState)

        lifecycleScope.launch(Dispatchers.Main) {
            when (currentState) {
                RNNSearchState.INIT -> {
                    viewBindingInstance.apply {
                        LLUserDetailsParent.visibility = View.GONE
                        FABFavoriteDetailsActivity.visibility = View.GONE
                        FABUnfavoriteDetailsActivity.visibility = View.GONE
                        TVStatusStringDetailsActivity.apply {
                            text = resources.getString(R.string.TVStatusStringDetailsActivity_INIT)
                            visibility = View.VISIBLE
                        }
                        IVStatusIconDetailsActivity.apply{
                            visibility = View.VISIBLE
                            Glide.with(this@DetailsActivity)
                                    .load(R.drawable.ic_baseline_grass_24)
                                    .into(this)
                        }
                        PBDetailsActivity.visibility = View.GONE
                    }
                }
                RNNSearchState.LOAD -> {
                    viewBindingInstance.apply {
                        LLUserDetailsParent.visibility = View.GONE
                        TVStatusStringDetailsActivity.apply{
                            text = when (contentLoadPhase) {
                                0 -> {
                                    resources.getString(R.string.TVStatusStringDetailsActivity_LOAD, currentRNNGithubUserMain.loginString.toString(), "user")
                                }
                                1 -> {
                                    resources.getString(R.string.TVStatusStringDetailsActivity_LOAD, currentRNNGithubUserMain.loginString.toString(), "follower")
                                }
                                2 -> {
                                    resources.getString(R.string.TVStatusStringDetailsActivity_LOAD, currentRNNGithubUserMain.loginString.toString(), "following")
                                }
                                else -> {
                                    resources.getString(R.string.TVStatusStringDetailsActivity_LOAD, currentRNNGithubUserMain.loginString.toString(), "data has went wrong thus leave no ")
                                }
                            }
                            visibility = View.VISIBLE
                        }
                        IVStatusIconDetailsActivity.visibility = View.GONE
                        FABFavoriteDetailsActivity.visibility = View.GONE
                        FABUnfavoriteDetailsActivity.visibility = View.GONE
                        PBDetailsActivity.visibility = View.VISIBLE
                    }

                    contentLoadPhase++
                }
                RNNSearchState.FINISH -> {
                    if (contentLoadPhase == 3) {

                        if (normalCreate) {
                            viewBindingInstance.apply {
                                LLUserDetailsParent.visibility = View.GONE
                                TVStatusStringDetailsActivity.apply {
                                    text = resources.getString(R.string.TVStatusStringDetailsActivity_FINISH)
                                    visibility = View.VISIBLE
                                }
                                IVStatusIconDetailsActivity.apply{
                                    visibility = View.VISIBLE
                                    Glide.with(this@DetailsActivity)
                                            .load(R.drawable.ic_baseline_done_outline_24)
                                            .into(this)
                                }
                                PBDetailsActivity.visibility = View.GONE
                            }

                            delay(300)
                        }

                        updateCurrentView()

                        viewBindingInstance.apply {
                            LLUserDetailsParent.visibility = View.VISIBLE
                            TVStatusStringDetailsActivity.visibility = View.GONE
                            IVStatusIconDetailsActivity.visibility = View.GONE
                            PBDetailsActivity.visibility = View.GONE
                        }
                    }
                }
                RNNSearchState.FAIL -> {
                    viewBindingInstance.apply {
                        LLUserDetailsParent.visibility = View.GONE
                        FABFavoriteDetailsActivity.visibility = View.GONE
                        FABUnfavoriteDetailsActivity.visibility = View.GONE
                        TVStatusStringDetailsActivity.apply {
                            text = resources.getString(R.string.TVStatusStringDetailsActivity_FAIL)
                            visibility = View.VISIBLE
                        }
                        IVStatusIconDetailsActivity.apply{
                            visibility = View.VISIBLE
                            Glide.with(this@DetailsActivity)
                                    .load(R.drawable.ic_baseline_sync_problem_24)
                                    .into(this)
                        }
                        PBDetailsActivity.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun updateCurrentView() {

        rnnSearchInstance.apply {
            getUser().observe(this@DetailsActivity, { details ->
                if (details != null) {
                    currentRNNGithubUserDetails = details
                    updateFABVisibility(details.loginString!!)

                    viewBindingInstance.apply {
                        with(details) {
                            Glide.with(this@DetailsActivity)
                                    .load(this.avatarString)
                                    .into(IVAvatarDetails)

                            TVFullnameDetails.text = this.nameString
                            TVUsernameDetails.text = this.loginString
                            TVLocationDetails.text = this.locationString
                            TVCompanyDetails.text = this.companyString
                            FABUnfavoriteDetailsActivity.apply {
                                setOnClickListener {
                                    rnnFavoriteInstance.add(RNNGithubUserFavorite(loginString = currentRNNGithubUserDetails.loginString!!, avatarString = currentRNNGithubUserDetails.avatarString!!))
                                    Snackbar.make(root.rootView, "Favorited", Snackbar.LENGTH_SHORT).show()
                                    updateFABVisibility(details.loginString!!)
                                }
                            }
                            FABFavoriteDetailsActivity.apply {
                                setOnClickListener {
                                    rnnFavoriteInstance.delete(RNNGithubUserFavorite(loginString = currentRNNGithubUserDetails.loginString!!, avatarString = currentRNNGithubUserDetails.avatarString!!))
                                    Snackbar.make(root.rootView, "Removed", Snackbar.LENGTH_SHORT).show()
                                    updateFABVisibility(details.loginString!!)
                                }
                            }
                        }
                    }
                }
            })
            getFollowers().observe(this@DetailsActivity, {
                currentRNNGithubUserFollower = it
            })
            getFollowing().observe(this@DetailsActivity, {
                currentRNNGithubUserFollowing = it
            })

            val viewPagerInstance: ViewPager2 = viewBindingInstance.VP2DetailsActivity
            viewPagerInstance.apply {
                adapter = DetailsActivitySectionsPagerAdapter(this@DetailsActivity, currentRNNGithubUserFollower, currentRNNGithubUserFollowing)
            }

            TabLayoutMediator(viewBindingInstance.TLDetailsActivity, viewPagerInstance) { tab, position ->
                tab.text = resources.getString(RNN_DETAILS_ACTIVITY_TAB_TITLES[position])
            }.attach()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun updateFABVisibility(currentLoginString: String) {
        with(rnnFavoriteInstance) {
            find(currentLoginString)
            getCurrentFavorite().observe(this@DetailsActivity, {
                if (it != null) {
                    viewBindingInstance.apply {
                        FABFavoriteDetailsActivity.visibility = View.VISIBLE
                        FABUnfavoriteDetailsActivity.visibility = View.GONE
                    }
                } else {
                    viewBindingInstance.apply {
                        FABFavoriteDetailsActivity.visibility = View.GONE
                        FABUnfavoriteDetailsActivity.visibility = View.VISIBLE
                    }
                }
            })
        }
    }
}