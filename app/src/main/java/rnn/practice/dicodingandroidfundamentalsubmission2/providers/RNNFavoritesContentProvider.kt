package rnn.practice.dicodingandroidfundamentalsubmission2.providers

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQueryBuilder
import rnn.practice.dicodingandroidfundamentalsubmission2.database.RNNGithubUserFavoriteRoomDatabase
import rnn.practice.dicodingandroidfundamentalsubmission2.entity.RNNGithubUserFavorite
import rnn.practice.dicodingandroidfundamentalsubmission2.interfaces.RNNGithubUserFavoriteDAO

class RNNFavoritesContentProvider : ContentProvider() {

    private lateinit var currentSupportSQLiteDatabaseWriteable: SupportSQLiteDatabase
    private lateinit var currentSupportSQLiteDatabaseReadable: SupportSQLiteDatabase
    private lateinit var currentRNNGithubUserFavoriteDAO: RNNGithubUserFavoriteDAO
    private val currentUriMatcherInstance = UriMatcher(UriMatcher.NO_MATCH)

    companion object {
        private const val AUTHORITY_STRING = "rnn.practice.dicodingandroidfundamentalsubmission2.providers.favorite"
        private const val BULK_MODE = 1
        private const val ID_MODE = 2
    }

    init {
        currentUriMatcherInstance.addURI(AUTHORITY_STRING, RNNGithubUserFavorite.tableName, BULK_MODE)
        currentUriMatcherInstance.addURI(AUTHORITY_STRING, "${RNNGithubUserFavorite.tableName}/", ID_MODE)
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val rowsDeletedCount: Int

        when (currentUriMatcherInstance.match(uri)) {
            BULK_MODE -> {
                rowsDeletedCount = currentSupportSQLiteDatabaseWriteable.delete(
                    RNNGithubUserFavorite.tableName,
                    selection,
                    selectionArgs
                )
            }
            ID_MODE -> {
                rowsDeletedCount = if (selection.isNullOrEmpty()) {
                    currentSupportSQLiteDatabaseWriteable.delete(
                        RNNGithubUserFavorite.tableName,
                        "${RNNGithubUserFavorite.primaryColumn}=${uri.lastPathSegment}",
                        null
                    )
                } else {
                    currentSupportSQLiteDatabaseWriteable.delete(
                        RNNGithubUserFavorite.tableName,
                        "${RNNGithubUserFavorite.primaryColumn}=${uri.lastPathSegment} AND $selection",
                        selectionArgs
                    )
                }
            }
            else -> throw IllegalArgumentException("This $uri for delete() is not available")
        }

        context?.contentResolver?.notifyChange(uri, null)

        return rowsDeletedCount
    }

    override fun getType(uri: Uri): String? {
        throw UnsupportedOperationException("getType() is not supported")
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return when (currentUriMatcherInstance.match(uri)) {
            BULK_MODE -> {

                if (values == null) {
                    return null
                }

                val currentId = currentSupportSQLiteDatabaseWriteable.insert(
                    RNNGithubUserFavorite.tableName,
                    SQLiteDatabase.CONFLICT_REPLACE,
                    values
                )

                context?.contentResolver?.notifyChange(uri, null)

                Uri.parse("${RNNGithubUserFavorite.tableName}/$currentId")
            }
            ID_MODE -> {
                throw IllegalArgumentException("Single insertion is not available")
            }
            else -> throw IllegalArgumentException("This $uri insertion is not available")
        }
    }

    override fun onCreate(): Boolean {
        context?.let {
            currentSupportSQLiteDatabaseWriteable = RNNGithubUserFavoriteRoomDatabase.checkDatabase(it).openHelper.writableDatabase
            currentSupportSQLiteDatabaseReadable = RNNGithubUserFavoriteRoomDatabase.checkDatabase(it).openHelper.readableDatabase
            currentRNNGithubUserFavoriteDAO = RNNGithubUserFavoriteRoomDatabase.checkDatabase(it).currentRNNGithubUserFavoriteDAO()
        }

        return false
    }

    override fun query(
        uri: Uri, projection: Array<String>?, selection: String?,
        selectionArgs: Array<String>?, sortOrder: String?
    ): Cursor? {
        return currentSupportSQLiteDatabaseReadable.query(SupportSQLiteQueryBuilder.builder(RNNGithubUserFavorite.tableName).selection(selection, selectionArgs).orderBy(sortOrder).create())
    }

    override fun update(
        uri: Uri, values: ContentValues?, selection: String?,
        selectionArgs: Array<String>?
    ): Int {
        val rowsUpdatedCount: Int

        when (currentUriMatcherInstance.match(uri)) {
            BULK_MODE -> {
                rowsUpdatedCount = currentSupportSQLiteDatabaseWriteable.update(
                    RNNGithubUserFavorite.tableName,
                    SQLiteDatabase.CONFLICT_REPLACE,
                    values,
                    selection,
                    selectionArgs
                )
            }
            ID_MODE -> {
                rowsUpdatedCount = if (selection.isNullOrEmpty()) {
                    currentSupportSQLiteDatabaseWriteable.update(
                        RNNGithubUserFavorite.tableName,
                        SQLiteDatabase.CONFLICT_REPLACE,
                        values,
                        "${RNNGithubUserFavorite.primaryColumn}=${uri.lastPathSegment}",
                        null
                    )
                } else {
                    currentSupportSQLiteDatabaseWriteable.update(
                        RNNGithubUserFavorite.tableName,
                        SQLiteDatabase.CONFLICT_REPLACE,
                        values,
                        "${RNNGithubUserFavorite.primaryColumn}=${uri.lastPathSegment} AND $selection",
                        selectionArgs
                    )
                }
            }
            else -> throw IllegalArgumentException("This $uri for update() is not available")
        }

        context?.contentResolver?.notifyChange(uri, null)

        return rowsUpdatedCount
    }
}