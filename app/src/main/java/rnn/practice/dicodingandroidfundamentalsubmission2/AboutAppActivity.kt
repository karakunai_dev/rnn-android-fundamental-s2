package rnn.practice.dicodingandroidfundamentalsubmission2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.ActivityAboutAppBinding

class AboutAppActivity : AppCompatActivity() {

    private lateinit var viewBindingInstance: ActivityAboutAppBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBindingInstance = ActivityAboutAppBinding.inflate(layoutInflater)
        setContentView(viewBindingInstance.root)

        supportActionBar?.title = resources.getString(R.string.ABAppAboutActivity)
    }
}