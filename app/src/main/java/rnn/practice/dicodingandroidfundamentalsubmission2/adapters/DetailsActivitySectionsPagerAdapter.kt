package rnn.practice.dicodingandroidfundamentalsubmission2.adapters

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import rnn.practice.dicodingandroidfundamentalsubmission2.fragments.FollowFragment
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain

class DetailsActivitySectionsPagerAdapter(activity: AppCompatActivity, vararg userDataFollows: ArrayList<RNNGithubUserMain>) : FragmentStateAdapter(activity) {

    private val dataSource = userDataFollows

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return FollowFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList("currentUserDataFollow", dataSource[position])
                putInt("currentFollowDataMode", position)
            }
        }
    }
}