package rnn.practice.dicodingandroidfundamentalsubmission2.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import rnn.practice.dicodingandroidfundamentalsubmission2.databinding.RnnGithubUserSearchItemBinding
import rnn.practice.dicodingandroidfundamentalsubmission2.models.RNNGithubUserMain

class RNNGithubUsersListAdapter(private val dataSource: ArrayList<RNNGithubUserMain>, private val stateListener: (RNNGithubUserMain) -> Unit) : RecyclerView.Adapter<RNNGithubUsersListAdapter.RNNGithubUsersViewHolder>() {

    inner class RNNGithubUsersViewHolder(private val viewBindingInstance: RnnGithubUserSearchItemBinding) : RecyclerView.ViewHolder(viewBindingInstance.root) {
        fun getCurrent() = viewBindingInstance
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RNNGithubUsersViewHolder {
        return RNNGithubUsersViewHolder(RnnGithubUserSearchItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RNNGithubUsersViewHolder, position: Int) {
        holder.apply {
            itemView.setOnClickListener { stateListener(dataSource[position]) }

            getCurrent().apply {

                Glide.with(itemView)
                        .load(dataSource[position].avatarString)
                        .apply(RequestOptions().override(128, 128))
                        .into(IVAvatarSearchItem)

                TVUsernameSearchItem.text = ("@${dataSource[position].loginString}")
            }
        }
    }

    override fun getItemCount(): Int = dataSource.size
}