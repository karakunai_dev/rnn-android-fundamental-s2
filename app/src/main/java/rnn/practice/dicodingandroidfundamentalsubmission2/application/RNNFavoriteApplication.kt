package rnn.practice.dicodingandroidfundamentalsubmission2.application

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import rnn.practice.dicodingandroidfundamentalsubmission2.database.RNNGithubUserFavoriteRoomDatabase
import rnn.practice.dicodingandroidfundamentalsubmission2.repository.RNNGithubUserFavoriteRepository

class RNNFavoriteApplication : Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    private val databaseInstance by lazy { RNNGithubUserFavoriteRoomDatabase.checkDatabase(this) }
    val repositoryInstance by lazy { RNNGithubUserFavoriteRepository(databaseInstance.currentRNNGithubUserFavoriteDAO()) }
}